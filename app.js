var corpo = document.body;
var barraLateral = document.querySelector(".l-sidebar");
var botaoMenu = document.querySelector(".l-sidebar__btn");

var boxeTexto = document.querySelector('.boxe_texto');
var secaoBemVindo = document.querySelector('.bg__profile');
var secaoSobreMim = document.querySelector('#sobre-mim');
var secaoContatos = document.querySelector('#contatos');

function fechar() {
  barraLateral.classList.add('l-sidebar--close')
};

fechar();

function abreFecha() {
  var une = barraLateral.classList[1];
  if (une == "l-sidebar--close") {
    barraLateral.classList.add('l-sidebar--open');
    barraLateral.classList.remove('l-sidebar--close');
  } else {
    barraLateral.classList.add('l-sidebar--close');
    barraLateral.classList.remove('l-sidebar--open');
  }
};

// PASSO 1 - Esta sessão deverá ser apresentada sempre que houver o click no botão Menu:
botaoMenu.addEventListener("click", abreFecha);

// PASSO 1.1 - Quando houver o click do usuário, a sidebar deverá ser apresentada:
barraLateral.addEventListener("click", abreFecha);

// PASSO 2 - O usuário poderá usar o teclado para abrir ou fechar a sidebar. No caso, a tecla Space:
corpo.addEventListener("keydown", (key) => {
  if (key.code == "Space") {
    abreFecha()
  }
})

// PASSO 3 - O usuário poderá usar o mouse para abrir ou fechar a sidebar. No caso, um duplo click em qualquer local do site irá abrir ou fechar a sidebar:
corpo.addEventListener("dblclick", abreFecha);

// 4. Um boxe de texto irá acompanhar o cursor do mouse.

document.body.addEventListener('mouseover', handleMouseOver);

function handleMouseOver() {
    handleMouseMove.boxeTexto = boxeTexto;
    this.addEventListener('mousemove', handleMouseMove);
}
  
const handleMouseLeave = {
    handleEvent() {
        this.element.removeEventListener('mousemove', handleMouseMove);
        this.element.removeEventListener('mouseleave', handleMouseLeave);
    }
}
  
const handleMouseMove = {
    handleEvent(e) {
        this.boxeTexto.style.top = e.clientY + 25 + 'px';
        this.boxeTexto.style.left = e.clientX + 13 +'px';
    }
}

/**
 * 4.1. O texto deverá mudar quando o cursor do mouse sobrepor qualquer uma das sessão do site:
 *
 *  - Receberá o texto `Bem vindo`.   
 *  - Receberá o texto `Sobre mim`.
 *  - Receberá o texto `Contatos`.
 * 
 */

secaoBemVindo.addEventListener('mouseover', function() {
  boxeTexto.innerText = 'Bem vindo';
  preventDefault();
})

secaoSobreMim.addEventListener('mouseover', function() {
  boxeTexto.innerText = 'Sobre mim';
  preventDefault();
})

secaoContatos.addEventListener('mouseover', function() {
  boxeTexto.innerText = 'Contatos';
  preventDefault();
})
